package code;

import javax.swing.*;
import code.MainGame;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The GUI class for use with MainGame.
 */
public class ClueGUI extends JFrame{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The window which contains all game elements.
	 */
	private JFrame window;
	
	/**
	 * The panel representing the tiled game board.
	 */
	private JPanel board;
	
	/**
	 * The panel representing the right half of the board.
	 */
	private JPanel playerContent;
	
	/**
	 * A 2-Dimensional array that stores the JLabels that represent board tiles.
	 */
	private JLabel[][] boardTiles = new JLabel[26][25];
	
	/**
	 * The panel representing the right half of the GUI which contains all player input. Holds
	 * checklistPanel, curPlayerLabel, and controlPanel. 
	 * 
	 * @see #checkListPanel
	 * @see #curPlayerLabel
	 * @see #controlPanel
	 */
	private JPanel playerPanels;
	
	/**
	 * The panel that holds buttons for player control(movement, options, etc.).
	 */
	private JPanel controlPanel;
	
	/**
	 * The panel used to indicate which players turn it is. Also used to provide instructions for
	 * suggestions and accusations.
	 */
	private JLabel curPlayerLabel;
	
	/**
	 * The panel used to display the current players 'checklist' of cards (known, in hand, and
	 * unknown).
	 * 
	 * @see #checklistButtons
	 */
	private JPanel checklistPanel;
	
	/**
	 * An array of buttons which represent the cards in the game. These buttons are updated to
	 * display the 'checklist' of the current player.
	 */
	private JButton[] checklistButtons = new JButton[21];
	
	/**
	 * The string value of a suspect chosen in an accusation or suggestion.
	 */
	private String sus;
	
	/**
	 * The string value of a weapon chosen in an accusation or suggestion.
	 */
	private String weap;
	
	/**
	 * The string value of a location chosen in an accusation or suggestion.
	 */
	private String loc;
	
	/**
	 * An array of colors representing the colors of the six game pieces. These colors are indexed
	 * in the same order as the players; i.e. index 0 == Miss Scarlet, index 1 == Professor Plum,
	 * etc.
	 */
	private Color[] playerColors = {
			new Color(248,2,1),	// Miss Scarlet
			new Color(118,0,110), // Professor Plum
			new Color(119,172,3), // Reverend Green
			new Color(254,248,230), // Mrs. White
			new Color(86,172,205), // Mrs. Peacock
			new Color(245,239,2) // Colonel Mustard
	};
	
	/**
	 * The color value used to represent gray room board tiles.
	 */
	private Color roomGray = new Color(183,180,173);
	
	/**
	 * The color value used to represent green hallway board tiles.
	 */
	private Color boardGreen = new Color(105,177,130);
	
	/**
	 * The instance of MainGame associated with this instance of ClueGUI.
	 * 
	 * @see MainGame
	 */
	private static MainGame game;
	
	/**
	 * Creates a new instance of ClueGUI associated with the given instance of MainGame.
	 * 
	 * @param mg The instance of MainGame associated with this instance of ClueGUI.
	 * @see MainGame
	 */
	public static void main(MainGame mg) {
		new ClueGUI(mg);
	}
	
	/**
	 * Assigns the given instance of MainGame to the instance variable game.
	 * 
	 * @param mg The instance of MainGame associated with this instance of ClueGUI.
	 * @see MainGame
	 * @see #game
	 */
	public ClueGUI(MainGame mg){
		game = mg;
		init();
	}
	
	/**
	 * Method for initialization of the GUI including JFrame, JPanels, and tiles. GUI initializes
	 * up to the player select state.
	 */
	private void init() {
		window = new JFrame("Clue");
		window.setSize(1280,720);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		//Main content panel. 2 columns: board, playerContent
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(1, 2));
		
		//Board initialization
		board = new JPanel();
		board.setLayout(new GridLayout(26, 25, 1, 1));
		board.setBackground(new Color(105,177,130));
		board.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		for (int i = 1; i < 26; i++) {
			for (int j = 1; j < 25; j++) {
				boardTiles[i - 1][j - 1] = new JLabel();
				JLabel currentPanel = boardTiles[i - 1][j - 1];
				currentPanel.setOpaque(true);
				board.add(currentPanel);
				tileUpdate(i, j);
			}
		}
		
		roomLabel(1, 1, "STUDY");
		roomLabel(2, 10, "HALL");
		roomLabel(2, 18, "LOUNGE");
		roomLabel(8, 0, "LIBRARY");
		roomLabel(13, 1, "POOL");
		roomLabel(14, 1, "ROOM");
		roomLabel(21, 0, "CONSER");
		roomLabel(22, 0, "VATORY");
		roomLabel(20, 8, "BALLROOM");
		roomLabel(11, 17, "DINING");
		roomLabel(12, 18, "ROOM");
		roomLabel(21, 18, "KITCHN");
		
		content.add(board);
		
		//Player Content panel. 2 rows: checklistPanel, controlPanel
		playerContent = new JPanel();
		playerContent.setLayout(new GridLayout(2, 0));
		
		
		//playerPanels
		playerPanels = new JPanel(new GridLayout(0, 2));
		playerContent.add(playerPanels);
		
		//curPlayerPanel
		curPlayerLabel = new JLabel();
		curPlayerLabel.setOpaque(true);
		playerPanels.add(curPlayerLabel);

		
		
		//Control Panel. Initial state: player count selection
		controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(0, 1));
		controlPanel.setBackground(boardGreen);
		JLabel playerSelectLabel = new JLabel("Select number of players:");
		playerSelectLabel.setHorizontalAlignment(SwingConstants.CENTER);
		controlPanel.add(playerSelectLabel);
		

		for (int i = 2; i < 7; i++) {
			JButton k=new JButton("" + i);
			k.addActionListener(new playerCountListener(i));
			controlPanel.add(k);			
		}
		
		//Checklist Panel. 3 Columns.
		initChecklist();
		
		playerPanels.add(controlPanel);
		
		content.add(playerContent);
		window.setContentPane(content);
		window.setVisible(true);
	}
	
	/**
	 * A function which (re)initializes the checklist panel.
	 * 
	 * @see #checklistPanel
	 */
	private void initChecklist() {
		checklistPanel = new JPanel();
		checklistPanel.setLayout(new GridLayout(0, 3));
		checklistPanel.setBackground(boardGreen);
		
		checklistPanel.add(new JButton("In Hand")).setForeground(Color.BLUE);;
		checklistPanel.add(new JButton("Known")).setForeground(Color.black);;
		checklistPanel.add(new JButton("Unknown")).setForeground(Color.gray);;
		for (int i = 0; i < 21; i++) {
			checklistButtons[i] = new JButton(game.cardMap.get(i));
			checklistPanel.add(checklistButtons[i]);
		}
		playerContent.add(checklistPanel);
	}

	/**
	 * The action listener used on the player count selection buttons. Allows for the selection of
	 * two to six players and upon selection, initializes those players in both MainGame and
	 * ClueGUI.
	 */
	public class playerCountListener implements ActionListener {
		
		/**
		 * The selected player count.
		 */
		private int pCount;
		
		/**
		 * Instantiates this ActionListener with the given player count.
		 * 
		 * @param i	An integer representing the player count to be set
		 */
		public playerCountListener(int i) {
			pCount = i;
		}
		
		/**
		 * Initializes the received player count and associated methods across the MainGame and
		 * ClueGUI instances.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			game.setPlayerCount(pCount);
			game.initPlayers(pCount);
			game.dealAll();
			for(int i=0;i<game.playerCount;i++){
				game.playerList.get(i).revealedCardSet();;
			}
			curPlayerLabelUpdate();
			hallDecision();
			checkListUpdate();
			controlPanel.revalidate();
			controlPanel.repaint();
		}
	}
	
	/**
	 * A method for updating the GUI representation of individual game board tiles given the tile's
	 * coordinates. These updates are based on the current state of game.gameBoard.
	 * 
	 * @param y	The 'uncorrected' y coordinate (game.gameBoard index value) of the tile to be
	 * 			changed.
	 * @param x	The 'uncorrected' x coordinate (game.gameBoard index value) of the tile to be
	 * 			changed.
	 * @see MainGame#gameBoard
	 */
	public void tileUpdate(int y, int x) {
		JLabel currentPanel = boardTiles[y - 1][x - 1];
		int currentTile = game.gameBoard[y][x];
		
		//	Out-of-bounds Tile
		if (currentTile == -4) {
			currentPanel.setBackground(boardGreen);
		}
		//	Wall/Room Tile
		if (currentTile == -3) {
			currentPanel.setBackground(roomGray);
		}
		//	Door Tile
		if (currentTile == -2) {
			currentPanel.setBackground(new Color(171,167,117));
		}
		//	Open Tile
		if (currentTile == -1) {
			currentPanel.setBackground(new Color(195,188,107));
		}
		//	Player
		if (currentTile >= 0 && currentTile < 6) {
			currentPanel.setBackground(playerColors[currentTile]);
		}
		currentPanel.repaint();
	}
	
	/**
	 * A method for updating the current player panel to display the current player's color and
	 * player number.
	 * 
	 * @see #curPlayerLabel
	 */
	public void curPlayerLabelUpdate() {
		curPlayerLabel.removeAll();
		if (game.playerCurrent.getLose()){
			curPlayerLabel.setText("Gameover");
		}
		else{
			curPlayerLabel.setText("Player " + (game.getPlayerCur() + 1) + "'s turn");
		}
		curPlayerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		curPlayerLabel.setBackground(playerColors[game.getPlayerCur()]);
		curPlayerLabel.revalidate();
		curPlayerLabel.repaint();
	}
	
	public void curPlayerLabelUpdate(String input) {
		curPlayerLabel.removeAll();
		curPlayerLabel.setText(input);
		curPlayerLabel.setHorizontalAlignment(SwingConstants.CENTER);
		curPlayerLabel.setBackground(playerColors[game.getPlayerCur()]);
		curPlayerLabel.revalidate();
		curPlayerLabel.repaint();
	}
	
	/**
	 * Updates the checklist buttons to display which cards are in the hand of, known, or unknown
	 * to the current player.
	 * 
	 * @see #checklistButtons
	 */
	public void checkListUpdate(){
		for(int i=0;i<checklistButtons.length;i++){
			if(game.playerCurrent.getRevealedCards(i)==false){
				checklistButtons[i].setForeground(Color.GRAY);
				
			}
			if(game.playerCurrent.getRevealedCards(i)==true){
				checklistButtons[i].setForeground(Color.black);
				
			}
			if(game.playerCurrent.hand.contains(i)){
				checklistButtons[i].setForeground(Color.BLUE);
				
			}
			checklistButtons[i].repaint();
		}
	}
	
	/**
	 * A method for labeling rooms. The string str is typed across tiles horizontally, one
	 * character per tile, starting at the given 'corrected' (boardTiles index) coordinates y and
	 * x.
	 * 
	 * @param y	The 'corrected' y coordinate of the tile which the label will start on.
	 * @param x	The 'corrected' x coordinate of the tile which the label will start on.
	 * @param str The room label to be set.
	 * 
	 * @see #boardTiles
	 */
	public void roomLabel(int y, int x, String str) {
		for (int i = 0; i < str.length(); i++) {
			String cur = "" + str.charAt(i);
			boardTiles[y][x + i].setText(cur);
			boardTiles[y][x + i].setHorizontalAlignment(SwingConstants.CENTER);
			boardTiles[y][x + i].setVerticalAlignment(SwingConstants.CENTER);
		}
	}
	
	/**
	 * A method for visually adding the current MainGame player to a given room.
	 * 
	 * @param room	The string value of the room which the player will be added to.
	 */
	public void addPlayerToRoom(String room) {
		Color curPlayerColor = playerColors[game.getPlayerCur()];
		if (room == "Study") {
			boardTiles[2][1 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Hall") {
			boardTiles[3][9 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Lounge") {
			boardTiles[3][18 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Dining Room") {
			boardTiles[13][17 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Kitchen") {
			boardTiles[22][18 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Ballroom") {
			boardTiles[21][9 + game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Conservatory") {
			boardTiles[23][game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Billiard Room") {
			boardTiles[15][game.getPlayerCur()].setBackground(curPlayerColor);
		}
		if (room == "Library") {
			boardTiles[9][game.getPlayerCur()].setBackground(curPlayerColor);
		}
	}
	
	/**
	 * A method for visually removing the current MainGame player from a given room.
	 * 
	 * @param room	The string value of the room which the player will be removed from.
	 */
	public void removePlayerFromRoom(String room) {
		if (room == "Study") {
			boardTiles[2][1 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Hall") {
			boardTiles[3][9 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Lounge") {
			boardTiles[3][18 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Dining Room") {
			boardTiles[13][17 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Kitchen") {
			boardTiles[22][18 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Ballroom") {
			boardTiles[21][9 + game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Conservatory") {
			boardTiles[23][game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Billiard Room") {
			boardTiles[15][game.getPlayerCur()].setBackground(roomGray);
		}
		else if (room == "Library") {
			boardTiles[9][game.getPlayerCur()].setBackground(roomGray);
		}
		else {}
	}
	
	/**
	 * The action listener for use on cards during the suggest phase.
	 */
	public class accCardListener implements ActionListener{
		
		/**
		 * The number associated with the card represented by this button.
		 */
		private int cardNum;
		
		/**
		 * Instantiates this action listener with the given card number.
		 * 
		 * @param i The number associated with the card represented by this button.
		 */
		public accCardListener(int card) {
			cardNum = card;
		}
		
		/**
		 * Sets the phase of the accusation based on the type of card value received.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if(cardNum>=0&&cardNum<=5){
					sus=game.cardMap.get(cardNum);
					acclistenSetup(1);
			}
			else if(cardNum>=6&&cardNum<=11){
				weap=game.cardMap.get(cardNum);
				acclistenSetup(2);
			}
			else if(cardNum>=12&&cardNum<=20){
				loc=game.cardMap.get(cardNum);
				acclistenSetup(3);
			}
		}
	}
	
	/**
	 * A method for setting up the card action listeners for an accusation phase.
	 * 
	 * @param type The phase of the accusation which to set up. Key: 0 = Suspect; 1 = Weapon; 2 =
	 * Room; 3 = Finalize accusation.
	 */
	private void acclistenSetup(int type) {
		/*
		for(int i=0;i<checklistButtons.length;i++){
			checklistButtons[i].removeAll();;
		}*/
		playerContent.remove(checklistPanel);
		initChecklist();
		checkListUpdate();
		if(type==0){
			curPlayerLabel.setText("Select a suspect");
			curPlayerLabel.repaint();
			for(int i=0;i<=5;i++){
				if(game.playerCurrent.getRevealedCards(i)==false){
					checklistButtons[i].addActionListener(new accCardListener(i));
				}
					
			}
		}
		else if(type==1){
			curPlayerLabel.setText("Select a weapon");
			curPlayerLabel.repaint();
			for(int i=6;i<=11;i++){
				if(game.playerCurrent.getRevealedCards(i)==false){
					checklistButtons[i].addActionListener(new accCardListener(i));
				}
			}
		}
		else if(type==2){
			curPlayerLabel.setText("Select a room");
			curPlayerLabel.repaint();
			for(int i=12;i<=20;i++){
				if(game.playerCurrent.getRevealedCards(i)==false){
					checklistButtons[i].addActionListener(new accCardListener(i));
				}
			}
		}
		else if(type==3){
			curPlayerLabel.setText(game.accuse(sus, weap, loc));
			curPlayerLabel.repaint();
			checkListUpdate();
			endTurnDecision();
		}
	}

	/**
	 * A method to update the controlPanel to reflect a beginning of turn decision state for the
	 * current player if they're not in a room.
	 * 
	 * @see #controlPanel
	 */
	public void hallDecision() {
		controlPanel.removeAll();
		JButton roll=new JButton("Roll the die");
		roll.addActionListener(new rollListener());
		controlPanel.add(roll);
		JButton Accuse=new JButton("Make an accusation");
		Accuse.addActionListener(new accuseListener());
		controlPanel.add(Accuse);
		if (game.gameBoardOriginal[game.playerCurrent.getYCord()][game.playerCurrent.getXCord()] == -2) {
			JButton enterRoom = new JButton("Enter Room");
			enterRoom.addActionListener(new moveListener("enter"));
			controlPanel.add(enterRoom);
		}
		controlPanel.revalidate();
		controlPanel.repaint();
	}
	
	/**
	 * The action listener used for the button that allows the player to start an accusation.
	 * 
	 * @author Donald Brant
	 */
	public class accuseListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			acclistenSetup(0);
		}
	}
	
	/**
	 * The action listener for the dice roll button.
	 */
	public class rollListener implements ActionListener{
		
		/**
		 * When the button is pressed the dice is rolled in the MainGame instance and the GUI
		 * proceeds to movement options.
		 * 
		 * @see #moveDecision
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			game.setRoll(game.rollDie());
			moveDecision();
		}
	}
	
	/**
	 * Initializes the control panel to allow the player to move within the hallway.
	 * 
	 * @see #controlPanel
	 */
	public void moveDecision(){
		controlPanel.removeAll();
		JButton up=new JButton("Up");
		JButton left=new JButton("Left");
		JButton right=new JButton("Right");
		JButton down=new JButton("Down");
		JButton endmove=new JButton("End Movement");
		JButton rollCount=new JButton(""+game.getRoll());
		if (game.gameBoardOriginal[game.playerCurrent.getYCord()][game.playerCurrent.getXCord()] == -2) {
			JButton enterRoom = new JButton("Enter Room");
			enterRoom.addActionListener(new moveListener("enter"));
			controlPanel.add(enterRoom);
		}
		up.addActionListener(new moveListener("up"));
		left.addActionListener(new moveListener("left"));
		right.addActionListener(new moveListener("right"));
		down.addActionListener(new moveListener("down"));
		endmove.addActionListener(new moveListener("over"));
		controlPanel.add(up);
		controlPanel.add(left);
		controlPanel.add(right);
		controlPanel.add(down);
		controlPanel.add(endmove);
		controlPanel.add(rollCount);
		controlPanel.revalidate();
		controlPanel.repaint();
	}
	
	/**
	 * A method that updates the control panel to give the player their options when their turn
	 * begins and they're inside a room.
	 * 
	 * @see #controlPanel
	 */
	public void roomDecision() {
		controlPanel.removeAll();
		JButton Accuse=new JButton("Make an accusation");
		Accuse.addActionListener(new accuseListener());
		controlPanel.add(Accuse);
		JButton suggest=new JButton("Make a suggestion");
		suggest.addActionListener(new suggestListener());
		controlPanel.add(suggest);
		JButton leave=new JButton("Leave by door");
		leave.addActionListener(new moveListener("leaveByDoor"));
		controlPanel.add(leave); //TODO: We need to know if there are multiple doors for the current room. Also, check if door is unblocked.
		String curRoom = game.playerCurrent.getRoom();
		if (curRoom == "Study" || curRoom == "Kitchen" ||  curRoom == "Conservatory" || curRoom == "Lounge") {
			JButton leaveByPassage = new JButton("Leave by secret passage");
			leaveByPassage.addActionListener(new moveListener("leaveByPassage"));
			controlPanel.add(leaveByPassage);
		}
		controlPanel.revalidate();
		controlPanel.repaint();
	}
	
	/**
	 * A method that updates the control panel to give the player their options after they've moved
	 * according to their dice roll.
	 * 
	 * @see #controlPanel
	 */
	public void endTurnDecision(){
		controlPanel.removeAll();
		if(game.playerCurrent.getLose()==false){
		JButton Accuse=new JButton("Make an accusation");
		Accuse.addActionListener(new accuseListener());
		controlPanel.add(Accuse);
		}
		JButton endturn=new JButton("End turn");
		endturn.addActionListener(new moveListener("end"));
		controlPanel.add(endturn);
		if (game.gameBoardOriginal[game.playerCurrent.getYCord()][game.playerCurrent.getXCord()] == -2) {
			JButton enterRoom = new JButton("Enter Room");
			enterRoom.addActionListener(new moveListener("enter"));
			controlPanel.add(enterRoom);
		}
		controlPanel.revalidate();
		controlPanel.repaint();
	}
	
	/**
	 * The action listener used for the button that allows the player to start a suggestion.
	 * 
	 * @author Zacory Demonterey
	 */
	public class suggestListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			suggListenSetup(0);
		}
	}

	/**
	 * A method for setting up the card action listeners for the suggestion phase.
	 * 
	 * @param type The phase of the suggestion which to set up. Key: 0 = Suspect; 1 = Weapon; 2 =
	 * Finalize suggestion.
	 */
	private void suggListenSetup(int type) {
		playerContent.remove(checklistPanel);
		initChecklist();
		checkListUpdate();
		if(type == 0) {
			curPlayerLabel.setText("Select a suspect");
			curPlayerLabel.repaint();
			for(int i = 0; i <= 5; i++){
				if(game.playerCurrent.getRevealedCards(i) == false) {
					checklistButtons[i].addActionListener(new suggCardListener(i));
				}
			}
		}
		else if(type == 1) {
			curPlayerLabel.setText("Select a weapon");
			curPlayerLabel.repaint();
			for(int i = 6; i <= 11; i++) {
				if(game.playerCurrent.getRevealedCards(i) == false){
					checklistButtons[i].addActionListener(new suggCardListener(i));
				}
			}
		}
		else if(type == 2){
			curPlayerLabel.setText(game.suggest(sus, weap));
			curPlayerLabel.repaint();
			checkListUpdate();
			endTurnDecision();
		}
	}
	
	/**
	 * The action listener for use on cards during the suggest phase.
	 * @author Zacory Demonterey
	 */
	public class suggCardListener implements ActionListener{
		
		/**
		 * The number associated with the card represented by this button.
		 */
		private int cardNum;

		/**
		 * Instantiates this action listener with the given card number.
		 * 
		 * @param i The number associated with the card represented by this button.
		 */
		public suggCardListener(int i) {
			cardNum = i;
		}

		/**
		 * Sets the phase of the suggestion based on the type of card value received.
		 */
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (cardNum >= 0 && cardNum <= 5) {
				sus=game.cardMap.get(cardNum);
				suggListenSetup(1);
			}
			else if (cardNum >= 6 && cardNum <= 11) {
				weap=game.cardMap.get(cardNum);
				suggListenSetup(2);
			}
		}
	}
	
	/**
	 * The action listener for movement commands.
	 * 
	 * @author Zacory Demonterey
	 *
	 */
	public class moveListener implements ActionListener{
		
		/**
		 * The input movement option.
		 */
		private String direct;
		
		/**
		 * Instantiates this move listener with the given input movement option.
		 * 
		 * @param input
		 */
		public moveListener(String input) {
			direct = input;
		}
		
		/**
		 * A method to check that the given value of the dice roll does not equal zero. When it
		 * does, endTurnDecision() is called.
		 * 
		 * @param roll The given value of the dice roll.
		 * 
		 * @see #endTurnDecision
		 */
		private void rollCheck(int roll){
			if(roll == 0){
				endTurnDecision();
			}
		}
		
		/**
		 * Attempts to apply the given movement.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			if(game.move(direct)) {
				game.setRoll(game.getRoll()-1);
				moveDecision();
				rollCheck(game.getRoll());
			}
			else if(direct=="over") {
				endTurnDecision();
			}
			else if(direct=="end") {
				if(game.playerCurrent.getLose()==true){
					confine();
				}
				game.nextPlayer();
				curPlayerLabelUpdate();
				checkListUpdate();
				if(game.playerCurrent.getRoom()=="None") {
					hallDecision();
				}
				else {
					roomDecision();
				}
			}
			else if (direct == "enter") {
				game.enterRoom();
				addPlayerToRoom(game.doorOf(game.playerCurrent.getYCord(), game.playerCurrent.getXCord()));
				tileUpdate(game.playerCurrent.getYCord(), game.playerCurrent.getXCord());
				game.setRoll(0);
				roomDecision();
			}
			else if (direct == "leaveByPassage") {
				removePlayerFromRoom(game.playerCurrent.getRoom());
				game.passageMove();
				addPlayerToRoom(game.playerCurrent.getRoom());
				game.nextPlayer();
				curPlayerLabelUpdate();
				checkListUpdate();
				if(game.playerCurrent.getRoom()=="None") {
					hallDecision();
				}
				else {
					roomDecision();
				}
			}
			else if (direct == "leaveByDoor") {
				//TODO: present doors that are unblocked as options
			}
		}
	}
	
	/**
	 * Moves the current player to the corner room closest to them. For use when a player makes
	 * an incorrect accusation and loses the game.
	 */
	private void confine() {
		int playerY = game.playerCurrent.getYCord();
		int playerX = game.playerCurrent.getXCord();
		if (playerY < 14 && playerX < 13) {
			game.playerCurrent.setRoom("Study");
			addPlayerToRoom("Study");
		}
		if (playerY < 14 && playerX >= 13) {
			game.playerCurrent.setRoom("Lounge");
			addPlayerToRoom("Lounge");
		}
		if (playerY > 14 && playerX < 13) {
			game.playerCurrent.setRoom("Conservatory");
			addPlayerToRoom("Conservatory");
		}
		if (playerY > 14 && playerX >= 13) {
			game.playerCurrent.setRoom("Kitchen");
			addPlayerToRoom("Kitchen");
		}
		game.gameBoard[playerY][playerX] = game.gameBoardOriginal[playerY][playerX];
		tileUpdate(playerY, playerX);
	}	
	
	/**
	 * Clears the control panel. For use in the winning or losing end game state.
	 * 
	 * @see #controlPanel
	 */
	public void clearControlPanel(){		
		playerPanels.remove(controlPanel);
		playerPanels.revalidate();
		playerPanels.repaint();
	}
}