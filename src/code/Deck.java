package code;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Manages a list of ints representing the deck.
 * 
 * @author Daniel Fox
 */
public class Deck {
	
	/**
	 * The int list of cards which this class controls.
	 */
	private ArrayList<Integer> deck;
	
	/**
	 * An ordered initialization of the deck with card 20 on top.
	 */
	public Deck() {
		deck = new ArrayList<Integer>(21);
		// initializing the deck to have values 0-20 at positions 0-20
		for (int i = 0; i<21 ; i++) {
			deck.add(i, i);
		}
	}
	
	/**
	 * Returns the current size of the deck. For use in debugging and testing.
	 * 
	 * @return	the size of the deck, e.g. 21 at the start of the game
	 */
	public int size(){
		return deck.size();
	}
	
	/**
	 * Returns the value of the card at a given position. For use in debugging and testing.
	 * 
	 * @param position	The position in the deck you are looking at, e.g. 0 for the bottom card.
	 * @return	The value of the card at the given position.
	 */
	public int peak(int position){
		return deck.get(position);
	}
	
	/**
	 * Returns the card on top of the deck and removes it from the deck.
	 * 
	 * @return	The int value of the card from the top of the deck.
	 */
	public int draw() {
		int drawn = deck.get(deck.size()-1);
		deck.remove(deck.size()-1);
		return drawn;
	}
	
	/**
	 * Returns true if there are any cards left in the deck.
	 * 
	 * @return	true if there are more cards remaining in the deck, false otherwise.
	 */
	public boolean hasNext() {
		if (deck.size()==0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	/**
	 * Resets the deck to its initial, unsorted state. For use in debugging and testing.
	 */
	public void reset(){
		deck.clear();
		for (int i = 0; i<21 ; i++) {
			deck.add(i, i);
		}
	}
	
	/**
	 * Shuffles the deck.
	 */
	public void shuffle() {
		// this method was added to make the syntax nicer, e.g. deck.shuffle()
		Collections.shuffle(deck);
	}
	
	/**
	 * Selects the person, weapon, and room out of the deck to represent the elements of the crime
	 * and returns them in an array.
	 * 
	 * @return	An array of ints of length three representing the elements of the crime.
	 */
	public int[] makeCrime() {
		int[] arrayOutput = new int[3];
		// picking a random person
		arrayOutput[0] = deck.get(randRange(0,5));
		// picking a random weapon
		arrayOutput[1] = deck.get(randRange(6,11));
		// picking a random room
		arrayOutput[2] = deck.get(randRange(12,20));
		//removing them now
		//deck.remove() works because deck haven't been shuffled yet
		deck.remove(arrayOutput[0]);
		deck.remove(arrayOutput[1]-1);
		deck.remove(arrayOutput[2]-2);
		return arrayOutput;
	}
	
	/**
	 * Returns a random int between the given ints lower and upper.
	 * 
	 * @param lower	An int for the lower bound (inclusive)
	 * @param upper	An int for the upper bound (inclusive)
	 * @return	A random int within the given range.
	 */
	public int randRange(int lower, int upper)
	{
	   int range = (upper - lower) + 1;     
	   return (int)(Math.random() * range) + lower;
	}
}
