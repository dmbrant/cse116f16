package code;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The representation of a player including their position on the board, their checklist, and their
 * hand of cards.
 * 
 * @author Daniel Fox
 * @author Donald Brant
 */
public class Player {
	
	/**
	 * Boolean that determines whether this player has lost game
	 */
	private boolean lost = false;
	
	/**
	 * This player's current Y coordinate.
	 */
	private int yCord;
	
	/**
	 * This player's current X coordinate.
	 */
	private int xCord;
	
	/**
	 * List of integers representing the cards dealt to this player.
	 * 
	 * @see cardMap
	 */
	public ArrayList<Integer> hand;
	
	/**
	 * List of cards revealed to the player. Cards indexed the same as cardMap. A value of true at
	 * a given index means that the card at that index has been revealed to this player.
	 * 
	 * @see cardMap
	 */
	public boolean[] revealedCards= new boolean[21];
	
	/**
	 * A reference to the game's cardMap. This will allow the player to convert thier card ints into
	 * the corresponding strings.
	 * 
	 * @see mainGame.cardMap
	 */
	private HashMap<Integer, String> cardMap;
	
	/**
	 * This player's current room. By default players aren't in any room.
	 */
	private String room = "None";
	
	/**
	 * A constructor that gives the player a reference to the games cardMap.
	 */
	public Player(HashMap<Integer, String> map) {
		cardMap = map;
		hand = new ArrayList<Integer>();
	}
	
	/**
	 * A parameterless constructor for player initialization that bypasses card handling for use in
	 * movement tests.
	 */
	public Player() {}
	
	/**
	 * Returns this player's Y coordinate.
	 * 
	 * @return {@link #yCord}
	 */
	public int getYCord() {
		return yCord;
	}
	
	/**
	 * Returns this player's X coordinate.
	 * 
	 * @return {@link #xCord}
	 */
	public int getXCord() {
		return xCord;
	}

	/**
	 * Sets this player's Y coordinate to the input value of y. Assumes y is a valid y location.
	 * 
	 * @param y Value to set as y coordinate.
	 */
	public void setYCord(int y) {
		yCord = y;
	}
	
	/**
	 * Sets this player's X coordinate to the input value of x. Assumes x is a valid x location.
	 * 
	 * @param x value to set as x coordinate.
	 */
	public void setXCord(int x) {
		xCord = x;
	}
	
	/**
	 * Takes an int representation of a card and adds it to the player's hand.
	 * 
	 * @param card The int representation of a card to be added to this player's hand.
	 * @see #hand
	 */
	public void dealt(int card) {
		int position = hand.size();
		hand.add(position, card);
	}
	
	/**
	 * Translates int values of cards into their corresponding String values.
	 * 
	 * @return	The string describing the card, e.g. input: 0; returns: "Miss Scarlett".
	 */
	public String mapCard(int input) {
		return cardMap.get(input);
	}
	
	/**
	 * Returns the value of the card at a given position in your hand.
	 * 
	 * @param position An int for the card's position in this player's hand
	 * @return The int value of the card at the requested position
	 */
	public int card(int position){
		return hand.get(position);
	}
	
	/**
	 * Returns the size of this player's hand. For use in debugging and testing.
	 * 
	 * @return	The size of the player's hand.
	 */
	public int handSize(){
		return hand.size();
	}
	
	/**
	 * Returns the ints in this player's hand as a string
	 * 
	 * @return	The int values in hand, separated by commas.
	 */
	public String hand(){
		return hand.toString();
	}
	
	/**
	 * Returns the cards in this player's hand as text in a string
	 * 
	 * @return	The cards in hand as strings, concatenated into one string, separated by spaces.
	 */
	public String mapHand() {
		String output = new String();
		for (int i=0; i<handSize(); i++){
			output = output + ' ' + mapCard(hand.get(i));
		}
		return output;
	}
	
	/**
	 * Returns the string value of this player's current room.
	 * 
	 * @return room
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * Sets this player's room to the string value newRoom. Assumes newRoom is a valid room value.
	 * 
	 * @param newRoom
	 */
	public void setRoom(String newRoom) {
		room = newRoom;
	}
	
	/**
	 * Initializes the revealedCards array by setting all values to false before adding players hands
	 * to the revealed cards list
	 */
	public void revealedCardSet(){
		for(int i=0;i<revealedCards.length;i++){
			revealedCards[i]=false;
		}
		for(int i=0;i<hand.size();i++){
			revealedCards[hand.get(i)]=true;
		}
	}
	
	/**
	 * Sets a card as known to this player.
	 * 
	 * @param x Integer representation of card to be set as known.
	 */
	public void setRevealedCard(int x){
		revealedCards[x]=true;
	}
	
	/**
	 * Returns whether selected card has been revealed to this player.
	 * 
	 * @param x Integer representation of card in question.
	 * @return boolean True if the card has been revealed to this player.
	 */
	public boolean getRevealedCards(int x){
		return revealedCards[x];
	}
	
	/**
	 * Returns true if the player has lost the game.
	 * 
	 * @return Whether the selected player has lost the game or not.
	 * @see #lost
	 */
	public boolean getLose(){
		return lost;
	}
	
	/**
	 * Sets the selected player to the lost state.
	 * 
	 * @see #lost
	 */
	public void loseGame(){
		lost=true;
	}
}
