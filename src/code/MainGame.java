package code;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * The central class for the game which holds all global game information and manages all game
 * behavior and assets.
 * 
 * @author Donald Brant
 * @author Zack Demonterey
 * @author Daniel Fox
 */
public class MainGame {
	
	/**
	 * Maps the ints representing cards into corresponding strings, e.g. 0, "Miss Scarlett".
	 */
	protected HashMap<Integer, String> cardMap = new HashMap<Integer, String>();
	
	/**
	 * The number of players in the current game.
	 */
	protected int playerCount = 0;
	
	/**
	 * List of player objects representing the players in the current game which is iterated
	 * through during play.
	 */
	protected ArrayList<Player> playerList = new ArrayList<Player>();
	
	/**
	 * The current player's index within playerList.
	 * 
	 * @see #playerList
	 */
	private int playerCur = 0;
	
	/**
	 * Deck of cards dealt to players before the first turn, implemented as list of ints.
	 * 
	 * @see Deck.deck()
	 * @see cardMap
	 */
	private Deck deck = new Deck();
	
	/**
	 * The three cards representing the crime.
	 * 
	 * @see #cardMap
	 */
	private int[] crime = new int[3];
	
	/**
	 * A reference to be updated to the player instance of the player who's turn it is currently.
	 */
	public Player playerCurrent;
	
	/**
	 * A two dimensional Array with each index representing a tile on the board. Additional
	 * 'padding' rows and columns have been added to represent the edge of the board for use in
	 * collision detection. 'Corrected' coordinates (excluding padding) are y = 25, x = 24.
	 *
	 * Use: int[Y-Coordinate][X-Coordinate].
	 *
	 * Value key:
	 * -1: No collision - free to move;
	 * -2: Door space - free to move, poll player if they'd like to enter the room;
	 * -3: Room - move blocked;
	 * -4: Out of bounds tile - move blocked;
	 * 0-5: Player - move blocked;
	 */
	public int[][] gameBoard =
		{
			{-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -4, -3, -3, -3, -3, -3, -3, -4, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -1, -1, -1, -1, -1, -2, -1, -2, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -2, -1, -1, -1, -1, -1, -4, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -2, -1, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -1, -4, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -2, -1, -2, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -2, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -4, -3},
			{-3, -4, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -3},
			{-3, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -4, -3},
			{-3, -4, -3, -3, -3, -3, -2, -1, -2, -3, -3, -3, -3, -3, -3, -3, -3, -2, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -4, -1, -1, -1, -3, -3, -3, -3, -1, -1, -1, -4, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -4, -4, -4, -1, -3, -3, -3, -3, -1, -4, -4, -4, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3}
		};
	
	/**
	 * A copy of the default gameBoard for use in resetting spaces after players move from them.
	 * 
	 * @see #gameBoard
	 */
	public final int[][] gameBoardOriginal = 
		{
			{-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -4, -3, -3, -3, -3, -3, -3, -4, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -1, -1, -1, -1, -1, -2, -1, -2, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -3, -1, -1, -2, -1, -1, -1, -1, -1, -4, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -1, -2, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -2, -1, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -1, -4, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -4, -2, -1, -2, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -2, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -2, -1, -1, -1, -1, -1, -1, -1, -1, -4, -3},
			{-3, -4, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -1, -2, -1, -1, -1, -1, -3},
			{-3, -1, -1, -1, -1, -1, -1, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -4, -3},
			{-3, -4, -3, -3, -3, -3, -2, -1, -2, -3, -3, -3, -3, -3, -3, -3, -3, -2, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3, -3, -1, -1, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -4, -1, -1, -1, -3, -3, -3, -3, -1, -1, -1, -4, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -4, -4, -4, -1, -3, -3, -3, -3, -1, -4, -4, -4, -3, -3, -3, -3, -3, -3, -3},
			{-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3}
		};
	
	/**
	 * The value given by the die roll. Set to a random value between one and six and decremented
	 * to zero for use as a tile move counter.
	 */
	private int roll;
	
	/**
	 * The instance of ClueGUI associated with this instance of MainGame.
	 */
	private ClueGUI gui;
	
	/**
	 * A constructor used to create a standard new instance of MainGame.
	 */
	public static void main(String[] args) {
		new MainGame();
	}
	
	/**
	 * Standard game initialization.
	 *
	 * @see #initMap()
	 * @see #initDeck()
	 * @see #initGUI()
	 */
	public MainGame() {
		initMap();
		initDeck();
		initGUI();
	}
	
	/**
	 * Constructor for testing and debugging.
	 * 
	 * @param	the cardMap
	 * @param	the number of players
	 * @param	the playerList
	 * @param	the gameBoard
	 * @param	the deck
	 * @param	the crime
	 */
	public MainGame(HashMap<Integer, String> cm, int pc, ArrayList<Player> pl, int[][] gb, Deck d, int[] c) {
		cardMap = cm;
		playerCount = pc;
		playerList = pl;
		gameBoard = gb;
		deck = d;
		crime = c;
	}
	
	/**
	 * Initializes cardMap with the key-value pairs for each card.
	 * 
	 * @see #cardMap
	 */
	public void initMap() {
		// People
		cardMap.put(0, "Miss Scarlett");
		cardMap.put(1, "Professor Plum");
		cardMap.put(2, "Reverend Green");
		cardMap.put(3, "Mrs. White");
		cardMap.put(4, "Mrs. Peacock");
		cardMap.put(5, "Colonel Mustard");
		// Weapons
		cardMap.put(6, "Candlestick");
		cardMap.put(7, "Dagger");
		cardMap.put(8, "Lead pipe");
		cardMap.put(9, "Revolver");
		cardMap.put(10, "Rope");
		cardMap.put(11, "Spanner");
		// Rooms
		cardMap.put(12, "Kitchen");
		cardMap.put(13, "Ballroom");
		cardMap.put(14, "Conservatory");
		cardMap.put(15, "Dining Room");
		cardMap.put(16, "Billiard Room");
		cardMap.put(17, "Library");
		cardMap.put(18, "Lounge");
		cardMap.put(19, "Hall");
		cardMap.put(20, "Study");
	}
	
	/**
	 * Updates the playerCount, instantiates that many instances of Player, adds those players to
	 * playerList, sets the starting coordinates for each player, and sets the starting value for
	 * both playerCur and playerCurrent.
	 * 
	 * @param pCount The number of players to initialize (between two and six).
	 * 
	 * @see #playerCount
	 * @see #playerCount
	 * @see #playerCur
	 * @see #playerCurrent
	 */
	public void initPlayers(int pCount) {
		/**Number of players in the game. Min value = 2, Max value = 6*/
		playerCount = pCount;
		int[] yStart = {6, 1, 8, 25, 25, 19};
		int[] xStart = {1, 17, 24, 15, 10, 1};
		for (int i=0; i<playerCount; i++){
			Player temp = new Player(cardMap);
			playerList.add(i,temp);
			playerCur = i;
			move(yStart[i], xStart[i]);
			gui.tileUpdate(yStart[i], xStart[i]);
		}
		playerCur = 0;
		playerCurrent = playerList.get(0);
	}
	
	/**
	 * Instantiates the instance of ClueGUI to be associated with this instance of MainGame.
	 */
	public void initGUI() {
		gui = new ClueGUI(this);
	}
	
	/**
	 * Creates the crime and shuffles the deck.
	 */
	public void initDeck() {
		crime = deck.makeCrime();
		for (int i = 0; i < 3; i++) {
			System.out.println(cardMap.get(crime[i]));
		}
		deck.shuffle();
	}
	
	/**
	 * Deals out all of the cards in the deck between all of the players in the game.
	 * 
	 * @see #dealOne(Player)
	 */
	public void dealAll() {
		int i = 0;
		int currentPlayer;
		while (deck.hasNext() == true) {
			currentPlayer = i % playerCount;
			dealOne(playerList.get(currentPlayer));
			i++;
		}
	}
	
	/**
	 * Deals one card to a given player.
	 * 
	 * @param player The player to whom a card is being dealt to.
	 */
	public void dealOne(Player player) {
		int card = deck.draw();
		player.dealt(card);
	}
	
	/**
	 * Resets the deck. For testing and debug use only.
	 */
	public void deckReset() {
		deck.reset();
	}
	
	/**
	 * A setter for the player count.
	 * 
	 * @param count The number of players being set.
	 * @see #playerCount
	 */
	public void setPlayerCount(int count) {
		playerCount = count;
	}
	
	/**
	 * Returns a random integer between one and six(inclusive)
	 * 
	 * @return A random integer between one and six(inclusive)
	 * @see #randRange(int, int)
	 */
	public int rollDie(){
		return randRange(1,6);
	}
	
	/**
	 * A method that returns a random integer value within a given upper an lower range.
	 * 
	 * @param lower The smallest possible value
	 * @param upper The largest possible value
	 * @return A random integer between lower and upper(inclusive)
	 */
	public int randRange(int lower, int upper) {
		int range = (upper - lower) + 1;     
	    return (int)(Math.random() * range) + lower;
	}
	
	/*
	 * End of setup methods
	 */
	
	/**
	 * Attempts to move the player by one tile in the direction given by input. Checks the proposed
	 * movement for collision, if the new tile is available, the players x or y coordinate is
	 * updated to that of the tile and the method returns true. If the tile is not available, the
	 * players coordinates are unchanged and the method returns false.
	 * 
	 * @param input  A string input of the direction to move the current player. Values: up, down,
	 * 				 left, or right.
	 * @return  true if the move was successfully made, false if not.
	 */
	public boolean move(String input) {
		int playerY = playerList.get(playerCur).getYCord();
		int playerX = playerList.get(playerCur).getXCord();
		
		gui.tileUpdate(playerY, playerX);
		if (input == "up"){
			if (moveCheck(playerY - 1, playerX)) {
				playerList.get(playerCur).setYCord(playerY - 1);
				gameBoard[playerY - 1][playerX] = playerCur;
				gameBoard[playerY][playerX] = gameBoardOriginal[playerY][playerX];
				gui.tileUpdate(playerY, playerX);
				gui.tileUpdate(playerY - 1, playerX);
				return true;
			}
			else {
				return false;
			}
		}
		else if (input == "down"){
			if (moveCheck(playerY + 1, playerX)) {
				playerList.get(playerCur).setYCord(playerY + 1);
				gameBoard[playerY + 1][playerX] = playerCur;
				gameBoard[playerY][playerX] = gameBoardOriginal[playerY][playerX];
				gui.tileUpdate(playerY, playerX);
				gui.tileUpdate(playerY + 1, playerX);
				return true;
			}
			else {
				return false;
			}
		}
		else if (input == "left"){
			if (moveCheck(playerY, playerX - 1)) {
				playerList.get(playerCur).setXCord(playerX - 1);
				gameBoard[playerY][playerX - 1] = playerCur;
				gameBoard[playerY][playerX] = gameBoardOriginal[playerY][playerX];
				gui.tileUpdate(playerY, playerX);
				gui.tileUpdate(playerY, playerX - 1);
				return true;
			}
			else {
				return false;
			}
		}
		else if (input == "right"){
			if (moveCheck(playerY, playerX + 1)) {
				playerList.get(playerCur).setXCord(playerX + 1);
				gameBoard[playerY][playerX + 1] = playerCur;
				gameBoard[playerY][playerX] = gameBoardOriginal[playerY][playerX];
				gui.tileUpdate(playerY, playerX);
				gui.tileUpdate(playerY, playerX + 1);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Moves the player to the tile given by the inputs yCord and xCord and updates the gameBoard
	 * to reflect that movement. This method is for use in setting initial player locations as well
	 * as for when players exit a room. This method assumes the given yCord and xCord are for a
	 * valid open tile.
	 * 
	 * @param yCord  The players new y coordinate
	 * @param xCord  The players new x coordinate
	 */
	public void move(int yCord, int xCord) {
		playerList.get(playerCur).setYCord(yCord);
		playerList.get(playerCur).setXCord(xCord);
		gameBoard[yCord][xCord] = playerCur;
	}

	/**
	 * Checks the validity of a proposed player tile movement by checking that the given yCord and
	 * xCord parameters don't represent a collision tile on the board.
	 *
	 * @param yCord  The proposed y coordinate of the moving player
	 * @param xCord  The proposed x coordinate of the moving player
	 * @return  True if the proposed move is valid (no collision).
	 */
	private boolean moveCheck(int yCord, int xCord) {
		return gameBoard[yCord][xCord] == -1 || gameBoard[yCord][xCord] == -2;
	}
	
	/**
	 * Moves the current player from one room to the room its connected with via secret passage.
	 * Only has an effect and should only be called if the player is in a room with a secret
	 * passage (Study, Kitchen, Conservatory, or Lounge).
	 */
	public void passageMove() {
		if (playerCurrent.getRoom() == "Study") {
			playerCurrent.setRoom("Kitchen");
		}
		else if (playerCurrent.getRoom() == "Kitchen") {
			playerCurrent.setRoom("Study");
		}
		else if (playerCurrent.getRoom() == "Conservatory") {
			playerCurrent.setRoom("Lounge");
		}
		else if (playerCurrent.getRoom() == "Lounge") {
			playerCurrent.setRoom("Conservatory");
		}
	}
	
	/**
	 * Moves the current player from a door tile to a room. Only has an effect and should only be
	 * called if the player is on a door tile (a tile who's gameBoardOrignal value == -2).
	 */
	public void enterRoom() {
		if (playerCurrent.getYCord() == 5 && playerCurrent.getXCord() == 7) {
			playerCurrent.setRoom("Study");
		}
		else if ((playerCurrent.getYCord() == 5 && playerCurrent.getXCord() == 9) ||
				(playerCurrent.getYCord() == 8 && playerCurrent.getXCord() == 12) ||
				(playerCurrent.getYCord() == 8 && playerCurrent.getXCord() == 13)) {
			playerCurrent.setRoom("Hall");
		}
		else if (playerCurrent.getYCord() == 7 && playerCurrent.getXCord() == 18) {
			playerCurrent.setRoom("Lounge");
		}
		else if ((playerCurrent.getYCord() == 9 && playerCurrent.getXCord() == 18) ||
				(playerCurrent.getYCord() == 13 && playerCurrent.getXCord() == 16)) {
			playerCurrent.setRoom("Dining Room");
		}
		else if (playerCurrent.getYCord() == 18 && playerCurrent.getXCord() == 20) {
			playerCurrent.setRoom("Kitchen");
		}
		else if ((playerCurrent.getYCord() == 20 && playerCurrent.getXCord() == 17) ||
				(playerCurrent.getYCord() == 17 && playerCurrent.getXCord() == 15) ||
				(playerCurrent.getYCord() == 17 && playerCurrent.getXCord() == 10) ||
				(playerCurrent.getYCord() == 20 && playerCurrent.getXCord() == 8)) {
			playerCurrent.setRoom("Ballroom");
		}
		else if (playerCurrent.getYCord() == 20 && playerCurrent.getXCord() == 6) {
			playerCurrent.setRoom("Conservatory");
		}
		else if ((playerCurrent.getYCord() == 16 && playerCurrent.getXCord() == 7) ||
				(playerCurrent.getYCord() == 12 && playerCurrent.getXCord() == 2)) {
			playerCurrent.setRoom("Billiard Room");
		}
		else if ((playerCurrent.getYCord() == 12 && playerCurrent.getXCord() == 4 ) ||
				(playerCurrent.getYCord() == 9 && playerCurrent.getXCord() == 8)) {
			playerCurrent.setRoom("Library");
		}
		gameBoard[playerCurrent.getYCord()][playerCurrent.getXCord()] = gameBoardOriginal[playerCurrent.getYCord()][playerCurrent.getXCord()];
	}
	
	/**
	 * Returns the ArrayList of players currently in the game.
	 * 
	 * @return  {@link #playerList}
	 */
	public ArrayList<Player> getPlayers() {
		return playerList;
	}
	
	/**
	 * Returns the index of the current player within the playerList.
	 * 
	 * @return	{@link #playerCur}
	 * @see #playerList
	 */
	public int getPlayerCur() {
		return playerCur;
	}
	
	/**
	 * Creates a new playerList containing the player newPlayer. For test purposes only.
	 * 
	 * @param newPlayer	The player to be added
	 */
	public void addPlayer(Player newPlayer) {
		playerList.add(newPlayer);
		playerCount++;
	}
	
	/**
	 * Allows players to suggest the criminal and weapon of the crime. Checks other players hands
	 * for cards to disprove the suggestion.
	 * 
	 * @param suspect  Suggested suspect
	 * @param weapon  Suggested weapon
	 * 
	 * @return  Room, weapon, or person card value revealed from another players hand that disproves
	 * 	        the suggestion, "No one could disprove your suggestion" if no other players could
	 * 			disprove the suggestion, or "Move to a room to make a suggestion" if the player is
	 * 			not in a room currently.
	 */
	public String suggest(String suspect, String weapon){
		//int equivalent of suspect
		int person=0;
		//int equivalent of weapon
		int thing=0;
		//int equivalent of room
		int place=0;
		String room=playerCurrent.getRoom();
		if(room!="None"){
			//Converts card Strings into their int equivalent
			for(int k=0;k<cardMap.size();k++){
				if(cardMap.get(k)==suspect){
					person=k;
				}
				else if(cardMap.get(k)==weapon){
					thing=k;
				}
				else if(cardMap.get(k)==room){
					place=k;
				}
			}
			//Checks each players hand for the desired values
			for(int j=0;j<playerList.size();j++){
				if(playerList.get(j).hand.contains(person)){
					playerCurrent.setRevealedCard(person);
					j=j+1;
					String player="Player "+j;
					return player+" reveals: "+suspect;
				}
				else if(playerList.get(j).hand.contains(thing)){
					playerCurrent.setRevealedCard(thing);
					j=j+1;
					String player="Player "+j;
					return player+ " reveals: "+weapon;
				}
				else if(playerList.get(j).hand.contains(place)){
					playerCurrent.setRevealedCard(place);
					j=j+1;
					String player="Player "+j;
					return player+" reveals: " +room;
				}
			}		
			return "No one could disprove your suggestion";
		}
		else {
			return"Move to a room to make a suggestion";
		}		
	}
	
	/**
	 * Allows a player to make an accusation and win or lose.
	 * 
	 * @param suspect Accused suspect
	 * @param weapon Weapon believed to be used
	 * @param room Room the crime is believed to be committed in
	 * 
	 * @return If wrong, Room, weapon, or person card value revealed from another players hand that disproves
	 * the accusation and the player who made the accusation loses the game.
	 * If correct, player who made accusation wins the game.
	 */
	public String accuse(String suspect, String weapon, String room){
		//int equivalent of suspect
		int person=0;
		//int equivalent of weapon
		int thing=0;
		//int equivalent of room
		int place=0;
		//Converts card Strings into their int equivalent
		for(int k=0;k<cardMap.size();k++){
			if(cardMap.get(k)==suspect){
				person=k;
			}
			else if(cardMap.get(k)==weapon){
				thing=k;
			}
			else if(cardMap.get(k)==room){
				place=k;
			}
		}
		//Checks each players hand for the desired values
		for(int j=0;j<playerList.size();j++){
			if(playerList.get(j).hand.contains(person)){
				playerList.get(j).setRevealedCard(person);
				playerCurrent.loseGame();
				playerCurrent.setRevealedCard(person);
				j=j+1;
				String player="Player "+j;
				return player+" reveals: "+suspect + ". Player " + (playerCur + 1) + " has lost.";
			}
			else if(playerList.get(j).hand.contains(thing)){
				playerList.get(j).setRevealedCard(thing);
				playerCurrent.loseGame();
				playerCurrent.setRevealedCard(thing);
				j=j+1;
				String player="Player "+j;
				return player+ " reveals: "+weapon + ". Player " + (playerCur + 1) +" has lost.";
			}
			else if(playerList.get(j).hand.contains(place)){
				playerList.get(j).setRevealedCard(place);
				playerCurrent.loseGame();
				playerCurrent.setRevealedCard(place);
				j=j+1;
				String player="Player "+j;
				return player+" reveals: " +room + ". Player " + (playerCur + 1) +" has lost.";
			}
		}
		gui.clearControlPanel();
		gui.roomLabel(10, 8, "PLAYER "+(1+playerCur));
		gui.roomLabel(11, 10, "WON");
		gui.roomLabel(12, 10, "THE");
		gui.roomLabel(13, 9, "GAME!");
		return "No one could disprove your accusation";
	}
	
	/**
	 * A setter for the value of the dice roll.
	 * 
	 * @param x The value to set {@link #roll} to.
	 */
	public void setRoll(int x) {
		roll = x;
	}
	
	/**
	 * A getter for the value of the dice roll.
	 * 
	 * @return {@link #roll}
	 */
	public int getRoll(){
		return roll;	
	}
	
	/**
	 * A method that sets playerCur and playerCurrent to values of the next player.
	 * 
	 * @see #playerCur
	 * @see #playerCurrent
	 */
	public void nextPlayer(){
		int lastp= playerCur;
		playerCur=(playerCur+1)%playerCount;
		playerCurrent=playerList.get(playerCur);
		while(playerCurrent.getLose()&&playerCur!=lastp){
			playerCur=(playerCur+1)%playerCount;
			playerCurrent=playerList.get(playerCur);
		}
		if(playerCur==lastp && playerCurrent.getLose()){
			gui.clearControlPanel();
			gui.roomLabel(10, 10, "ALL");
			gui.roomLabel(11, 8, "PLAYERS");
			gui.roomLabel(12, 9, "LOSE!");
			}
	}
	
	
	/**
	 * A method that takes a y and x coordinate and determines which room that tile is a door
	 * of. This method should only be called for door tiles (when gameBoardOriginal == -2).
	 * 
	 * @param y The y-coordinate of the door tile in question
	 * @param x The x-coordinate of the door tile in question
	 * @return The room name that this door leads to.
	 */
	public String doorOf(int y, int x) {
		//This implementation is a little caveman, which I'm aware of but I'll compress later
		//For now I'd like to have it this way so I can reverse the process for a doorsClear function
		if (y == 5 && x == 7) {
			return "Study";
		}
		if (y == 5 && x == 9) {
			return "Hall";
		}
		if (y == 7 && x == 18) {
			return "Lounge";
		}
		if (y == 8 && x == 12) {
			return "Hall";
		}
		if (y == 8 && x == 13) {
			return "Hall";
		}
		if (y == 9 && x == 8) {
			return "Library";
		}
		if (y == 9 && x == 18) {
			return "Dining Room";
		}
		if (y == 12 && x == 2) {
			return "Billiard Room";
		}
		if (y == 12 && x == 4) {
			return "Library";
		}
		if (y == 13 && x == 16) {
			return "Dining Room";
		}
		if (y == 16 && x == 7) {
			return "Billiard Room";
		}
		if (y == 17 && x == 10) {
			return "Ballroom";
		}
		if (y == 17 && x == 15) {
			return "Ballroom";
		}
		if (y == 18 && x == 20) {
			return "Kitchen";
		}
		if (y == 20 && x == 6) {
			return "Conservatory";
		}
		if (y == 20 && x == 8) {
			return "Ballroom";
		}
		if (y == 20 && x == 17) {
			return "Ballroom";
		}
		else {
			return "None";
		}
	}
}