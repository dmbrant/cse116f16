    "Explain how to see that a player can enter a suggestion;
     Explain how to see that a suggestion will have to include a weapon & person and forces the player to use the room they are in;
     Explain how to see that a suggestion can only be made when a player is in a room."
     
Launch MainGame and select the number of players.

Roll the die and move the player towards a room. Upon enter the room, the player is given the
option to "Make a suggestion." Choosing this option will make the text "Player #'s turn" turn into
instructions for making a suggestion. First the player must select a suspect, clicking on weapons
and locations has no effect at this point. Then the player must select a weapon. Clicking on a
person at this point will change your pick for a person but the suggestion isn't submitted until
the player selects a weapon. From there the suggestion is checked across the cards that the other
players hold in hand in an attempt to find one card that disproves it. If one is found, the message
"Player # reveals: CardName" appears. If the suggestion can't be disproved, the message "No one
could disprove your suggestion" is displayed. The suggestion option is only given when a player is
in a room and at no time during the suggestion is the player able to pick a room to suggest. The
players current room is automatically used in the suggestion.