package tests;
import static org.junit.Assert.*;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;

import code.Player;

public class PlayerTest {
	private HashMap<Integer, String> cardMap;
	
	@Before
	public void initMap() {
		// initializing the cardMap from ints to strings
		cardMap = new HashMap<Integer, String>();
		// putting values into cardMap
		// first for people
		cardMap.put(0, "Miss Scarlett");
		cardMap.put(1, "Professor Plum");
		cardMap.put(2, "Mrs. Peacock");
		cardMap.put(3, "Reverend Green");
		cardMap.put(4, "Colonel Mustard");
		cardMap.put(5, "Mrs. White");
		// then weapons
		cardMap.put(6, "Candlestick");
		cardMap.put(7, "Dagger");
		cardMap.put(8, "Lead pipe");
		cardMap.put(9, "Revolver");
		cardMap.put(10, "Rope");
		cardMap.put(11, "Spanner");
		// then rooms
		cardMap.put(12, "Kitchen");
		cardMap.put(13, "Ballroom");
		cardMap.put(14, "Conservatory");
		cardMap.put(15, "Dining Room");
		//cardMap.put(16, "Cellar");
		cardMap.put(16, "Billiard Room");
		cardMap.put(17, "Library");
		cardMap.put(18, "Lounge");
		cardMap.put(19, "Hall");
		cardMap.put(20, "Study");
	}
	
	@Test
	public void dealtTest() {
		Player p1 = new Player(cardMap);
		int card = 5;
		p1.dealt(card);
		assertTrue(p1.card(0) == 5);
		p1.dealt(6);
		p1.dealt(7);
		p1.dealt(8);
		assertTrue(p1.handSize() == 4);
	}
	
	@Test
	public void mapCardTest() {
		Player p1 = new Player(cardMap);
		String str = p1.mapCard(0);
		assertTrue(str.equals("Miss Scarlett"));
	}
	@Test
	public void handTest() {
		Player p1 = new Player(cardMap);
		p1.dealt(0);
		p1.dealt(0);
		p1.dealt(0);
		String hand = p1.hand();
		assertTrue(hand.equals("[0, 0, 0]"));
		// this should be the right format
		// if not, then you might need import java.util.Arrays;
		// and then use Arrays.toString(hand) in Player for hand()
	}
	@Test
	public void cardTest() {
		Player p1 = new Player(cardMap);
		p1.dealt(0);
		p1.dealt(1);
		p1.dealt(2);
		assertTrue(p1.card(2)==2);
	}
	@Test
	public void handSizeTest() {
		Player p1 = new Player(cardMap);
		p1.dealt(0);
		p1.dealt(1);
		p1.dealt(2);
		assertTrue(p1.handSize()==3);
	}
	@Test
	public void mapHandTest(){
		Player p1 = new Player(cardMap);
		p1.dealt(0);
		p1.dealt(1);
		String output = p1.mapHand();
		String test = new String();
		test = " Miss Scarlett Professor Plum";
		assertTrue(output.equals(test));
		//System.out.print(output);
		//System.out.print("\n");
		//System.out.print(output);
	}
}
