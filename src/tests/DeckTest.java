package tests;
import static org.junit.Assert.*;
import org.junit.Test;

import code.Deck;

public class DeckTest {
	@Test
	public void initTest() {
		Deck deck = new Deck();
		assertTrue(deck.size()==21);
		int first = deck.draw();
		int second = deck.draw();
		assertTrue(first==20);
		assertTrue(second==19);
	}
	@Test
	public void randRangeTest() {
		Deck deck = new Deck();
		int out = deck.randRange(1, 10);
		assertTrue(out >=1 & out<=10);
		int out2 = deck.randRange(27, 58);
		assertTrue(out2 >=27 & out2<=58);
	}
	@Test
	public void crimeTest() {
		Deck deck = new Deck();
		int[] output = deck.makeCrime();
		assertTrue(output[0] >= 0 & output[0] <= 5);
		assertTrue(output[1] >= 6 & output[1] <= 11);
		assertTrue(output[2] >= 12 & output[1] <= 20);
		//assertTrue(output[0] >= 12 & output[0] <= 20);
	}
	@Test
	public void shuffleTest() {
		Deck deck = new Deck();
		int card1 = deck.peak(20);
		deck.shuffle();
		int card2 = deck.draw();
		assertTrue(card1==20);
		assertFalse(card2==20);
		//assertTrue(card2==20);
	}
	
	@Test
	public void hasNextTest() {
		Deck deck = new Deck();
		assertTrue(deck.hasNext());
		for (int i =1 ; i <= 21; i++) {
			deck.draw();
		}
		assertFalse(deck.hasNext());
	}
	@Test
	public void sizeTest() {
		Deck deck = new Deck();
		assertTrue(deck.size()==21);
		deck.draw();
		assertTrue(deck.size()==20);
	}
	@Test
	public void peakTest() {
		Deck deck = new Deck();
		assertTrue(deck.peak(0)==0);
		assertTrue(deck.peak(15)==15);
	}

}
