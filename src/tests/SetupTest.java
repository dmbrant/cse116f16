package tests;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;

import code.Deck;
import code.MainGame;
import code.Player;

public class SetupTest {
	private HashMap<Integer, String> cardMap = new HashMap<Integer, String>();
	private int playerCount = 0;
	private ArrayList<Player> playerList = new ArrayList<Player>();
	private Deck deck = new Deck();
	private int[] crime = new int[3];
	// this one still needs init
	private int[][] gameBoard = new int[25][24];
	public MainGame clue;
	
	@Before
	public void pseudoConstructor(){
		clue = new MainGame(cardMap, playerCount, playerList, gameBoard, deck, crime);
	}
	
	@Test
	public void mapTest() {
		clue.initMap();
		//String outputInner = setup.cardMap.get(0);
		String outputOuter = cardMap.get(0);
		//System.out.print(outputInner);
		//System.out.print(outputOuter);
		assertTrue(outputOuter.equals("Miss Scarlett"));
	}
	
	@Test
	public void addPieces() {
		//nothing here yet
	}
		
	@Test
	public void dealOneTest() {
		// setting up for 1 player
		clue.initMap();
		clue.initPlayers(1);
		Player p1 = playerList.get(0);
		// testing dealOne() to 1 player
		clue.dealOne(p1);
		assertTrue(p1.card(0)==20);
		assertTrue(deck.size()==20);
	}
	
	@Test
	public void dealAllTest() {
		// setting up for 2 players
		clue.deckReset();
		clue.initPlayers(2);
		// testing dealAll() for 2 players
		clue.dealAll();
		Player p1 = playerList.get(0);
		Player p2 = playerList.get(1);
		assertTrue(p1.card(0)==20);
		assertTrue(p2.card(0)==19);
		//System.out.println(deck.size());
		assertFalse(deck.hasNext());
		assertTrue(p1.handSize()==11);
		assertTrue(p2.handSize()==10);
		
		// setting up for 3 players
		clue.deckReset();
		clue.initPlayers(3);
		//setup.initDeck();
		// testing dealAll() for 3 players
		clue.dealAll();
		p1 = playerList.get(0);
		p2 = playerList.get(1);
		Player p3 = playerList.get(2);
		assertTrue(p1.card(0)==20);
		assertTrue(p1.card(1)==17);
		assertTrue(p2.card(0)==19);
		assertTrue(p3.card(0)==18);
		assertFalse(deck.hasNext());
		assertTrue(p1.handSize()==7);
		assertTrue(p2.handSize()==7);
		assertTrue(p3.handSize()==7);
	}
}
