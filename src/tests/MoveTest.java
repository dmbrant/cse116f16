package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import code.MainGame;
import code.Player;

/**
 * @author Donald Brant
 */
public class MoveTest {

	/*
	 * Our means for player movement involves a loop which runs until the value of the dice roll is
	 * decremented to 0. Each time a successful move is made (the 'move()' method returns true) the
	 * dice roll value is decremented by 1. The following tests bypass this game loop and directly
	 * test the move method and its effects on player coordinates per move.
	 */
	
	MainGame mg;
	Player cur;
	
	@Before
	public void initialize() {
		mg = new MainGame();
		cur = new Player();
		mg.addPlayer(cur);
	}
	
	@Test
	public void horizontalTest() {
		System.out.println("\n horizontalTest(): \n");
		//	(8, 17) - An open space at the far left of the longest horizontal stretch on the board
		cur.setYCord(17);
		cur.setXCord(7);
		//	Move 12 spaces to the right
		for (int i = 8; i < 20; i++) {
			assertTrue(mg.move("right"));
			assertEquals(17, cur.getYCord());
			assertEquals(i, cur.getXCord());
			if (mg.gameBoard[17][i] == -2) {
				System.out.println("y = 17 x = " + i + " is a door!");
			}
		}
		//	Move 12 spaces to the left
		for (int i = 18; i > 6; i--) {
			assertTrue(mg.move("left"));
			assertEquals(17, cur.getYCord());
			assertEquals(i, cur.getXCord());
			if (mg.gameBoard[17][i] == -2) {
				System.out.println("y = 17 x = " + i + " is a door!");
			}
		}
	}

	@Test
	public void verticalTest() {
		System.out.println("\n verticalTest(): \n");
		//	(8, 2) - An open space at the top of the longest vertical stretch on the board
		cur.setYCord(2);
		cur.setXCord(8);
		//	Move 12 spaces down
		for (int i = 3; i < 15; i++) {
			assertTrue(mg.move("down"));
			assertEquals(i, cur.getYCord());
			assertEquals(8, cur.getXCord());
			if (mg.gameBoard[i][2] == -2) {
				System.out.println("y = " + i + " x = 2 is a door!");
			}
		}
		//	Move 12 spaces up
		for (int i = 13; i > 0; i--) {
			assertTrue(mg.move("up"));
			assertEquals(i, cur.getYCord());
			assertEquals(8, cur.getXCord());
			if (mg.gameBoard[i][2] == -2) {
				System.out.println("y = " + i + " x = 2 is a door!");
			}
		}
	}
	
	@Test
	public void horizontalVerticalTest() {
		System.out.println("\n horizontalVerticalTest(): \n");
		//	(6, 5) - An open space at the top of an eight tile zig-zag
		cur.setYCord(5);
		cur.setXCord(6);
		System.out.println(" Down and to the right:\n");
		//	Moving down and to the right
		for (int i = 6; i < 10; i++) {
			assertTrue(mg.move("down"));
			assertEquals(i, cur.getYCord());
			assertEquals(i, cur.getXCord());
			System.out.println("y = " + cur.getYCord() + " x = " + cur.getXCord());
			//while (i < 9) {
			if (mg.move("right")) {
				assertEquals(i, cur.getYCord());
				assertEquals(i + 1, cur.getXCord());
			}
			System.out.println("y = " + cur.getYCord() + " x = " + cur.getXCord());
		}
		System.out.println("\n Up and to the left:\n");
		//	Moving up and to the left
			for (int i = 8; i > 4; i--) {
				assertTrue(mg.move("up"));
				assertEquals(i, cur.getYCord());
				assertEquals(i + 1, cur.getXCord());
				System.out.println("y = " + cur.getYCord() + " x = " + cur.getXCord());
				if (mg.move("left")) {
					assertEquals(i, cur.getYCord());
					assertEquals(i, cur.getXCord());
				}
				System.out.println("y = " + cur.getYCord() + " x = " + cur.getXCord());
			}
	}
	
	@Test
	public void doorToRoomTest() {
		mg.playerCurrent = cur;
		//	Unblocked doors are represented by a 1 on the game board.
		assertEquals(-2, mg.gameBoard[5][7]);
		//	By default the player isn't in any room
		assertEquals("None", cur.getRoom());
		//	Player lands on door space
		cur.setYCord(5);
		cur.setXCord(7);
		//	Player chooses to enter door
		mg.enterRoom();
		//	Check that the player is in the right room
		assertEquals("Study", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[5][9]);
		cur.setYCord(5);
		cur.setXCord(9);
		mg.enterRoom();
		assertEquals("Hall", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[7][18]);
		cur.setYCord(7);
		cur.setXCord(18);
		mg.enterRoom();
		assertEquals("Lounge", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[9][18]);
		cur.setYCord(9);
		cur.setXCord(18);
		mg.enterRoom();
		assertEquals("Dining Room", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[18][20]);
		cur.setYCord(18);
		cur.setXCord(20);
		mg.enterRoom();
		assertEquals("Kitchen", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[20][17]);
		cur.setYCord(20);
		cur.setXCord(17);
		mg.enterRoom();
		assertEquals("Ballroom", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[20][6]);
		cur.setYCord(20);
		cur.setXCord(6);
		mg.enterRoom();
		assertEquals("Conservatory", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[16][7]);
		cur.setYCord(16);
		cur.setXCord(7);
		mg.enterRoom();
		assertEquals("Billiard Room", cur.getRoom());
		
		assertEquals(-2, mg.gameBoard[12][4]);
		cur.setYCord(12);
		cur.setXCord(4);
		mg.enterRoom();
		assertEquals("Library", cur.getRoom());
		
	}
	
	/*
	 * In the game loop, at the beginning of a players turn, if they're in a room that has a secret
	 * passageway they'll be asked if they'd like to take it. If they take it passageMove() is then
	 * called. It detects which passage room they're in and the corresponding room to warp them to.
	 */
	@Test
	public void secretPassageTest() {
		mg.playerCurrent = cur;
		cur.setRoom("Study");
		mg.passageMove();
		assertEquals("Kitchen", cur.getRoom());
		mg.passageMove();
		assertEquals("Study", cur.getRoom());
		cur.setRoom("Lounge");
		mg.passageMove();
		assertEquals("Conservatory", cur.getRoom());
		mg.passageMove();
		assertEquals("Lounge", cur.getRoom());
	}

	@Test
	public void tooManyTilesTest() {
		/*
		 * Our means of player movement involves a while loop that decrements for each successful
		 * single tile move made, i.e. players can only make as many moves as they're alloted by
		 * their dice roll.
		 */
	}
	
	@Test
	public void diagonalTest() {
		/*
		 * Due to our movement implementation, players can only move one tile at a time and they
		 * can only move to available unoccupied non-wall tiles adjacent to their current tile.
		 * Diagonal movements aren't possible.
		 */
	}

	@Test
	public void contiguousTest() {
		/*
		 * Due to our four direction implementation of movement, skipping spaces and thus making
		 * non-contiguous moves is impossible. In all instances of standard dice-based player
		 * movement it is only ever legal for players to move one tile in either the x OR y
		 * direction for each single move alloted by the dice roll.
		 */ 
	}
	
	@Test
	public void wallCollisionTest() {
		//	(1, 6) - A starting space with wall tiles above and below and the board edge to the left
		cur.setYCord(6);
		cur.setXCord(1);
		assertFalse(mg.move("up"));
		assertEquals(6, cur.getYCord());
		assertEquals(1, cur.getXCord());
		assertFalse(mg.move("left"));
		assertEquals(6, cur.getYCord());
		assertEquals(1, cur.getXCord());
		assertFalse(mg.move("down"));
		assertEquals(6, cur.getYCord());
		assertEquals(1, cur.getXCord());
		assertTrue(mg.move("right"));
		assertEquals(6, cur.getYCord());
		assertEquals(2, cur.getXCord());
		
		//	(8, 1) - An open tile with a wall tile to the right
		cur.setYCord(2);
		cur.setXCord(9);
		assertFalse(mg.move("right"));
		assertEquals(2, cur.getYCord());
		assertEquals(9, cur.getXCord());
	}

	/*
	 * At the beginning of each players 'move' game state the tile at the players coordinates is
	 * reset to the boards default value for that tile (set from 2 to either 0 or 1). At the end of
	 * each players 'move' game state, if they moved to a tile on the board, the move is committed
	 * to the board by means of setting that tile of the gameboard to a value of 2 which is the
	 * collision value that is also used for walls and the edge of the board. The following test
	 * exemplifies this functionality outside of the game loop(which is dependent on player input).
	 */
	@Test
	public void playerCollsionTest() {
		//	(12, 8) - A door space with wall tiles above and below it
		cur.setYCord(8);
		cur.setXCord(12);
		assertEquals(-2, mg.gameBoard[8][12]);
		//	A player ends their move on tile (12, 8) and decides not to go into the room from this door
		//	The board gets written a value of 2 for that tile to indicate collision.
		mg.gameBoard[cur.getYCord()][cur.getXCord()] = 2;
		//	Next player. (Treating cur as a separate player here)
		cur.setYCord(8);
		cur.setXCord(11);
		assertFalse(mg.move("right"));
	}
	// these two tests may need to be changed to test the IO versions of these methods
	@Test
	public void randRangeTest() {
		int out = mg.randRange(1, 10);
		assertTrue(out >=1 & out<=10);
		int out2 = mg.randRange(27, 58);
		assertTrue(out2 >=27 & out2<=58);
	}
	@Test
	public void rollDieTest() {
		int roll = mg.rollDie();
		assertTrue(roll >=1 & roll<=6);
	}
}
