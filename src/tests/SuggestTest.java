package tests;

import static org.junit.Assert.*;
import org.junit.Test;

import code.MainGame;
import code.Player;


public class SuggestTest extends MainGame {
	private Player p1=new Player(cardMap);
	private Player p2=new Player(cardMap);
	private Player p3=new Player(cardMap);
	//Sets up test conditions
	private void testSetup(){
		initMap();
		playerCount=3;
		p1.setRoom("Study");
		//first players hand
		p1.hand.add(0);
		p1.hand.add(6);
		p1.hand.add(12);
		//second players hand
		p2.hand.add(1);
		p2.hand.add(7);
		p2.hand.add(13);
		//third players hand
		p3.hand.add(2);
		p3.hand.add(8);
		p3.hand.add(14);
		addPlayer(p1);
		addPlayer(p2);
		addPlayer(p3);
		}
	//Test for when next player holds relevant suspect card
	@Test
	public void nextPlayerSuspectTest(){
		testSetup();
		assertEquals(suggest("Professor Plum","Dagger"),"Player 2 reveals: Professor Plum");
	}
	//Test for when next player holds relevant room card
	@Test
	public void nextPlayerRoomTest(){
		testSetup();
		p1.setRoom("Ballroom");
		assertEquals(suggest("Mrs. White","Spanner"),"Player 2 reveals: Ballroom");
	}
	//Test for when next player holds relevant weapon card
	@Test
	public void nextPlayerWeaponTest(){
		testSetup();
		assertEquals(suggest("Mrs. White","Dagger"),"Player 2 reveals: Dagger");
	}
	//Test for when next player holds more then one relevant card
	@Test
	public void nextPlayerTwoMatchTest(){
		/* nextPlayerSuspectTest already deals with this.
		In the case of multiple matches cards will
		be revealed in the order of suspect>weapon>room*/
	}
	//Test for when player after next player holds a relevant card
	@Test
	public void afterPlayerTest(){
		testSetup();
		assertEquals(suggest("Reverend Green","Rope"),"Player 3 reveals: Reverend Green");
		
	}
	//Test for when turn player holds the relevant card
	@Test
	public void PlayerSelfTest(){
		testSetup();
		assertEquals(suggest("Miss Scarlett","Lead pipe"),"Player 1 reveals: Miss Scarlett");
	}
	//Test for when turn player is the only player with a relevant card
	@Test
	public void PlayerSelfOnlyTest(){
		testSetup();
		assertEquals(suggest("Colonel Mustard","Candlestick"),"Player 1 reveals: Candlestick");
	}
	//Test for when no player holds a card to disprove the suggestion
	@Test
	public void noMatch(){
		testSetup();
		assertEquals(suggest("Colonel Mustard","Rope"),"No one could disprove your suggestion");
	}
	//Test for when player attempts to make suggestion while not in room
	@Test
	public void noRoom(){
		testSetup();
		p1.setRoom("None");
		assertEquals(suggest("Colonel Mustard","Rope"),"Move to a room to make a suggestion");
		
	}
}
