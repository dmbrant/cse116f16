Meeting Minutes for 10/18/16

Meeting Attendance: (list all students who were present) [1 minute]
	M- Daniel Fox
	Zac Zac Demonterey

User stories completed since last meeting [1-2 minutes]
	Players can now hold a hand of cards
	Players can be dealt cards
	Test classes for Deck, Player, and MainGame were added

User stories worked upon but not completed since last meeting [2-4 minutes]
	Player Movement
	Suggestion mechanic

Goals for the next meeting [20+ minutes]
	Player Movement JUnit done(tentatively)
	Suggestion Mechanic JUnit

Schedule for the next week's set of pair programming meetings [3-4 minutes]
	Zac, Daniel, and Donald - Friday 9/23 - 4PM - Capen 3rd, by elevators
	Accuse and Suggestion Mechanic Discussion
	Player movement
	Win Conditions
