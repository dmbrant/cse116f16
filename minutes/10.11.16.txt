Meeting Minutes for 10/11/16

Meeting Attendance: (list all students who were present) [1 minute]
	M- Daniel Fox
	Zac Zac Demonterey
	Donald Brant

User stories completed since last meeting [1-2 minutes]
	Creating a crime
	Shuffling the deck

User stories worked upon but not completed since last meeting [2-4 minutes]
	Card Handout
	Player Movement

Goals for the next meeting [20+ minutes]
	Card Handout JUnit done(tentatively)
	Player Movement JUnit done(tentatively)
	Suggestion Mechanic JUnit

Schedule for the next week's set of pair programming meetings [3-4 minutes]
	Zac, Daniel, and Donald - Friday 9/23 - 4PM - Capen 3rd, by elevators
	User stories and features to implement:
		Accuse and Suggestion Mechanic Discussion
		Dealing mechanic
		Deck and Player Test classes
		Tests for initialization and setup of MainGame
